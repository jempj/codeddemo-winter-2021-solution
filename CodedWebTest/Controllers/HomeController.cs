﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CodedWebTest.Data;
using Microsoft.AspNetCore.Mvc;
using CodedWebTest.Entities;
using CodedWebTest.Services;
using CodedWebTest.Models;

namespace CodedWebTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly WebTestDBContext _ctx;
        private readonly ISessionDataService _sessionData;

        public HomeController(WebTestDBContext webTestDBContext, ISessionDataService sessionDataService) 
        {

            _ctx = webTestDBContext; // Completed: Get WebTestDBContext via Dependency Injection
            _sessionData = sessionDataService; // Completed: Get SessionData via Dependency Injection


        }

        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> CheckEmail(IndexModel profile) //COMPLETED: Check if email exists in the database
        {
            try
            {
                //If the form field is valid (Email address)
                if (ModelState.IsValid)
                {

                    //Using the context (so the connection closes when concluded)
                    using (var context = _ctx)
                    {

                        //Compare the value we got from the form to the context, see if there's an existing value
                        var emailValidation = context.EmailAddress.Where(i => i.Address == profile.EmailAddress).FirstOrDefault();

                        //If the email is not currently in the db, display a message that says so
                        if (emailValidation != null) return Json(new { message = "This email is presently in the database!", status = "Information" });
                        // Else, return this message if the form value WASN'T in the incoming form submission
                        else { return Json(new { message = "The email is currently not in the database.", status = "Information" });

                        }

                    }

                }
                //If the user didn't enter in a valid email address, warn them about it
                else
                {
                    return Json(new { message = "Please Enter a Valid Email Address.", status = "Error" });
                }
            }

            //If something fails during the email lookup, display an error message
            catch (Exception e)
            {
                return Json(new { message = "Oops! There was an error processing your request. Please try again!", status = "Error" });

            }
        }

        
        public async Task<IActionResult> SaveEmail(IndexModel profile) //COMPLETED: Save email to the database and user session
        {

            try
            {
                //Did they submit a valid email?
                if (ModelState.IsValid)
                {

                    //Using the context (so the connection closes when concluded)
                    using (var context = _ctx)
                    {

                        //Check to see if the exising email is in the db (context
                        var validateEmail = context.EmailAddress.Where(i => i.Address == profile.EmailAddress).FirstOrDefault();


                        //Did we find an existing email?
                        if (validateEmail != null)
                        {
                            //Set it to the session anyway
                            _sessionData.EmailAddress = validateEmail.Address;

                            //Display message that the email is already in the db, and that email has been added to the session
                            return Json(new { message = "The Email Address is already in the database. The user has been loaded into the session.",
                                status = "Information" });
                          
                        }

                        ///If it's a new email, prepare a new object with the following params
                        var newEmailAddress = new EmailAddress()
                        {
                            Address = profile.EmailAddress, //incoming email address submitted
                            CreatedDate = DateTime.Now, //Set the created date to current (now)
                            EmailAddressUid = Guid.NewGuid() //Unique ID for email

                        };

                        //Finally, add the email entry and save it to the context
                        context.EmailAddress.Add(newEmailAddress);
                        context.SaveChanges();

                        //last but not least, set the email address to the current context
                        _sessionData.EmailAddress = newEmailAddress.Address;
                    }

                    //Return message stating that email has been entered into the db / sessiono
                    return Json(new { message = "Your Email has been entered into the database and session successfully!", status = "Sucess!" });
                }
                {
                    //If the email is an invalid format, display this message
                    return Json(new { message = "Please Enter a Valid Email Address.", status = "Error" });
                }

            }
            catch(Exception e)
            {
                //An erorr occured while processing the email in the context (or otherwise)
                return Json(new { message = "Oops! There was an error processing your email. Please try again!", status = "Error" });
            }

        }

   
        public async Task<IActionResult> GetEmail(IndexModel profile)  // COMPLETED: Get saved email from session
        {
            try
            {

                //Set the message depending on the session.
               //Message for if the session is currently empty
               var initialMessage = String.IsNullOrEmpty(_sessionData.EmailAddress) ? "There is currently no email address stored in the session" 
                    //return a message if the session string is filled
                    : $"The current email in the session is: {_sessionData.EmailAddress}";

                //Return our message from above
                return Json(new { message = initialMessage, status = "Information" });
            }

            //Exception if we cannot successfully retrieve the session
            catch (Exception e)
            {
                return Json(new { message = "Oops! There was an error processing your request. Please try again!", status = "Error" });
            }

        }

    }
}
